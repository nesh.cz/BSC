
import controller.MainController;
import java.math.BigDecimal;
import model.MainModel;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author petrh
 */
public class PaymentUnitTest {

    /**
     * Test updateValue method of Values class
     */
    @Test
    public void testUpdateValueFunction() {
	MainModel values = new MainModel();

	values.updateCurrencyBalance("CZK", new BigDecimal(100));
	values.updateCurrencyBalance("CZK", new BigDecimal(-100));
	values.updateCurrencyBalance("CZK", new BigDecimal(10));
	values.updateCurrencyBalance("CZK", new BigDecimal(-10));
	values.updateCurrencyBalance("CZK", new BigDecimal(-100));
	values.updateCurrencyBalance("CZK", new BigDecimal(100));
	values.updateCurrencyBalance("CZK", new BigDecimal(99));
	assert (values.getCurrencyValue("CZK").compareTo(new BigDecimal(99)) == 0);

	values.updateCurrencyBalance("USD", new BigDecimal(99));
	values.updateCurrencyBalance("USD", new BigDecimal(99));
	assert (values.getCurrencyValue("USD").compareTo(new BigDecimal(198)) == 0);

	values.updateCurrencyBalance("GBP", new BigDecimal(15));
	values.updateCurrencyBalance("GBP", new BigDecimal(-400));
	assert (values.getCurrencyValue("GBP").compareTo(new BigDecimal(-385)) == 0);

	values.updateCurrencyBalance("EUR", new BigDecimal(400));
	assert (values.getCurrencyValue("EUR").compareTo(new BigDecimal(400)) == 0);
    }

    /**
     * Test checkAndSavePayment method of MainController class
     */
    @Test
    public void testCheckAndSavePayment() {

    }
}
