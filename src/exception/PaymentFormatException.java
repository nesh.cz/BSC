package exception;

/**
 *
 * @author petrh
 */
public class PaymentFormatException extends Exception {

    public PaymentFormatException() {
	super();
    }

    public PaymentFormatException(String message) {
	super(message);
    }
}
