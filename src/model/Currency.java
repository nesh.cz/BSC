package model;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 *
 * @author petrh
 */
public class Currency {

    private String currencyCode;
    private BigDecimal usdExchangeRate;
    private BigDecimal actualBalance;  // Best practicies for money

    public Currency() {
    }

    /**
     * @param currencyCode the currencyCode to set
     */
    public Currency(String currencyCode) {
	this.currencyCode = currencyCode;
	this.actualBalance = new BigDecimal(0);
    }

    /**
     * @param currencyCode the currencyCode to set
     * @param actualBalance the actualBalance to set
     */
    public Currency(String currencyCode, BigDecimal actualBalance) {
	this.currencyCode = currencyCode;
	this.actualBalance = actualBalance;
    }

    /**
     * @return the currencyCode
     */
    public String getCode() {
	return currencyCode;
    }

    /**
     * @param currencyCode the currencyCode to set
     */
    public void setCode(String currencyCode) {
	this.currencyCode = currencyCode;
    }

    /**
     * @return the usdExchangeRate
     */
    public BigDecimal getUsdExchangeRate() {
	return usdExchangeRate;
    }

    /**
     * @param usdExchangeRate the usdExchangeRate to set
     */
    public void setUsdExchangeRate(BigDecimal usdExchangeRate) {
	this.usdExchangeRate = usdExchangeRate;
    }

    /**
     * @return the actualBalance
     */
    public BigDecimal getActualBalance() {
	return actualBalance;
    }

    /**
     * @return the actualBalance in USD
     */
    public BigDecimal getActualBalanceInUSD() {
	return actualBalance.multiply(usdExchangeRate);
    }

    /**
     * @param inputValue the inputValue to add to the actualBalance
     */
    public void updateBalance(BigDecimal inputValue) {
	actualBalance = actualBalance.add(inputValue);
    }

    public boolean isGreaterThanZero() {
	return (getActualBalance().compareTo(BigDecimal.ZERO) > 0);
    }

    /**
     * @return true if the usd exchange rate is set, false if the usd echange
     * rate is not set
     */
    public boolean isSetUsdRate() {
	return (usdExchangeRate != null);
    }

    /**
     * @return String with actual balance and balance converted to USD (if rate
     * is set)
     */
    public String getActualBalanceString() {
	StringBuilder sb = new StringBuilder();
	DecimalFormat df = new DecimalFormat("#.00");
	sb.append(currencyCode);
	sb.append(" ");
	sb.append(df.format(actualBalance));

	if (isSetUsdRate()) {
	    sb.append(" (USD ");
	    sb.append(df.format(getActualBalanceInUSD()));
	    sb.append(")");
	}

	sb.append("\r\n");
	return sb.toString();
    }

}
